# library management

- offline system. Libriarian in a hman tur

- member registration
    - expiry nei thei
    - renewal
    - ID card

- books
    - list
    - rent
    - rent due date
    - late fee
    - status

### Functional requirements
- Authentation
    - App will make sure only a librarian can have access.
    - It will require email and password to login
- Book management
    - A librarian can add books.
    - A librarian can Update and delete books
- search books.
    - A librarian can search for books based on titles, publication date, author etc., and also find their location in the library.
- reserve/renew
    - A librarian can reserve books and also renew books.
- notify overdue
    - The librarian will be notified if there is an over due books.
- calculate fine 
    - The app will calculate the fine for overdue books.

### Database
- user
    
    - Id
    - name
    - email
    - password
    - contact_number
    - address

- subscription
    
    - member_Id
    - start_date
    - expiry_date
    - user_id

- member

    - user_id
    - current_sub_id
    - sub_status
    - contact_number
    - gender
    - issued books

- Borrow

    - member_id
    - book_Id
    - rent_due_date
    - fine
    - rent_date
    - return_date
- books

    - title
    - shelf
    - current_borrow_id
    - status
    - category
    - author

- ID card

    - id
    - member_id
    - enrollment_id
    - path


