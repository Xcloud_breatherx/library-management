import { api } from "src/boot/axios";
const { ref } = require("vue");

const currentAcademicSession = ref(null);
const academicSessions = ref([]);
const loggedIn = ref(false);
const user = ref(null);

const login = async (credentials) => {
  try {
    // errors.value = {}
    await api.get("sanctum/csrf-cookie");
    const res = await api.post("/auth/login", credentials);
    user.value = res.data.user;
    currentAcademicSession.value = res.data.currentAcademicSession;
    academicSessions.value = res.data.academicSessions;
    loggedIn.value = true;
  } catch (error) {
    throw error;
  }
  return true;
};
const checkLoginStatus = async () => {
  try {
    const res = await api.get("/user");

    user.value = res.data.user;
    currentAcademicSession.value = res.data.currentAcademicSession;
    academicSessions.value = res.data.academicSessions;
    loggedIn.value = true;
  } catch (error) {
    user.value = null;
    loggedIn.value = false;
  }
};
const logout = async () => {
  try {
    await api.post("auth/logout");
    user.value = null;
    loggedIn.value = false;
  } catch (error) {
    throw error;
  }

  return true;
};
const setLoggedIn = (status) => {
  loggedIn.value = status;
};
const setUser = (user) => {
  user.value = user;
};
const setCurrentAcademicSession = (session) => {
  currentAcademicSession.value = session;
};

const useAuth = () => {
  return {
    currentAcademicSession,
    academicSessions,
    loggedIn,
    user,
    login,
    checkLoginStatus,
    logout,
    setLoggedIn,
    setUser,
    setCurrentAcademicSession,
    useAuth,
  };
};

export { useAuth };
