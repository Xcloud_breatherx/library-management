import { useQuasar, date } from "quasar";

const { api } = require("src/boot/axios");
const { ref } = require("vue");

const $q = useQuasar();

const rows = ref([]);
const loading = ref(false);
const pagination = ref({
  sortBy: "name",
  descending: false,
  page: 1,
  rowsPerPage: 15,
  rowsNumber: 0,
});

const filter = ref({
  name: "",
  person_in_charge: "",
  bialtu_hming: "",
});

const columns = [
  {
    name: "id",
    label: "ID",
    field: "id",
    sortable: true,
  },
  {
    name: "name",
    label: "Hming",
    field: "name",
    sortable: true,
  },
  {
    name: "person_in_charge",
    label: "Bialtu",
    field: (row) =>
      row.bialtu ? row.bialtu.name + " (" + row.bialtu.id + ")" : "",
    // field: function (row) {
    //     return row.bialtu.name + ' (' + row.bialtu.id + ')'
    // }
  },
  {
    name: "created",
    label: "Created At",
    field: (row) => date.formatDate(row.created_at, "YYYY-MM-DD"),
  },
  {
    name: "actions",
  },
];

const fetchData = async (props) => {
  // const response = await api.get("areas", {params});
  // areas.value = response.data;
  const { page, rowsPerPage, sortBy, descending } = props.pagination;
  const paginationFilter = props.filter;

  const params = props.pagination;

  try {
    const res = await api.get("areas", { params: params });

    rows.value = res.data.data;
    pagination.value.rowsNumber = res.data.total;
    pagination.value.rowsPerPage = res.data.per_page;
    pagination.value.page = res.data.current_page;
    pagination.value.sortBy = sortBy;
    pagination.value.descending = descending;
  } catch (error) {
    $q.notify(error.message);
  }
};

const useAreas = function () {
  return {
    rows,
    columns,
    filter,
    pagination,
    loading,
    fetchData,
  };
};

export { useAreas };
