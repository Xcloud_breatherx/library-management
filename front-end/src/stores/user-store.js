import { defineStore } from 'pinia';
import { server } from 'src/boot/axios';

export const useUserStore = defineStore('user', {
  state: () => ({
    id: null,
    name: null,
    email: null,
  }),
  getters: {
    getId: (state) => state.id,
    getName: (state) => state.name,
    getEmail: (state) => state.email,
  },
  actions: {
    async getSanctumCookie () {
      try {
        await server.get('sanctum/csrf-cookie')
      } catch (error) {
        if(error) throw error
      }
    },

    async login (email, password) {
      try {
        await server.post('login', { email, password })
      } catch (error) {
        if(error) throw error
      }
    },

    async fetchUser () {
      try {
        return await server.get('api/user')
      } catch (error) {
        if(error) throw error
      }
    },

    setUser (payload) {
      if (payload.id) this.id = payload.id
      if (payload.name) this.name = payload.name
      if (payload.email) this.email = payload.email
    },

    clearUser () {
      this.id = null
      this.name = null
      this.email = null
    }
  },
});
