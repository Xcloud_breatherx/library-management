const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/IndexPage.vue") },
      {
        path: "profile",
        component: () => import("pages/Auth/ProfilePage.vue"),
      },
      { path: "areas", component: () => import("pages/Areas/AreasList.vue") },

      { path: "members", component: () => import("pages/Members/MembersList.vue") },


      {
        path: "areas-composable",
        component: () => import("pages/Areas/AreasComp.vue"),
      },
      {
        path: "certificates/generate",
        component: () => import("pages/Certificates/GenerateCertificates.vue"),
      },
    ],
  },
  {
    path: "/auth",
    component: () => import("layouts/AuthLayout.vue"),
    children: [
      { path: "login", component: () => import("pages/Auth/LoginPage.vue") },
      {
        path: "forgot-password",
        component: () => import("src/pages/Auth/LoginPage.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
