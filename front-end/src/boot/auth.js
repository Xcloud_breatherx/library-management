// const { useAuthStore } = require("src/stores/auth");

import { useAuth } from "src/scripts/use-auth";

const { checkLoginStatus } = useAuth();

export default async () => {
  await checkLoginStatus();
};
