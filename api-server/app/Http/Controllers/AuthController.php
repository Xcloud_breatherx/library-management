<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function showRegistrationForm()
    {
        return view('auth.register');
    }
    public function login(LoginRequest $request)
    {
        if(Auth::attempt($request->except(['_token']))) {
            if ($request->expectsJson()) {
                return [
                    'user' => Auth::user(),

                ];
            }
            return redirect('/admin');
        }
        if ($request->expectsJson()) {
            throw ValidationException::withMessages(['email' => 'Email/password combination not found']);
        }
    }


    public function register(RegisterRequest $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        User::create($data);
        return redirect('/auth/login')
            ->with('messageSuccess', 'Account created. Please use your email and password to login');
    }


}
