<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMembersRequest;
use App\Models\Member;
use App\Models\Members;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MembersController extends Controller
{
    public function index()
    {
        if (request() -> input('filter')) {
            $filter = json_decode(request() -> input('filter'));

            if ($filter->name) {
                $members = Members :: where('name', 'like', '%' . $filter -> name . '%');
            }
        }

        if (request()->expectsJson()) {
            $sortBy = request()->input('sortBy', 'created_at');
            $sortOrder = request()->input('descending') == 'true' ? 'desc' : 'asc';
            $members = $members->orderBy($sortBy, $sortOrder);
            return $members->paginate(request()->input('rowsPerPage'));
        }
        $viewData = [
            'members' => $members->get()
        ];
        return view('members.index', $viewData);
    }


    public function store(StoreMembersRequest $request)
    {

            info($request->all());
            Members::store($request);

    }


