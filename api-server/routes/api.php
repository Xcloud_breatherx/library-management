<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\MembersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('sanctum/csrf-cookie', function () { return 'ok';});
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();


});
Route::group([
    'prefix' => 'auth',
], function() {
    Route::post('login', [AuthController::class, 'login']);
    // Route::post('register', [AuthController::class, 'register']);


    // Route::get('forgot-password', [AuthController::class, 'showForgotPassword']);
    // Route::post('forgot-password', [AuthController::class, 'sendForgotPasswordEmail']);
    // Route::get('reset-password/{token}/{email}', [AuthController::class, 'showResetPassword'])->name('password.reset');
    // Route::post('reset-password', [AuthController::class, 'resetPassword']);

    // Route::get('/google-sign-in', [AuthController::class, 'loginWithGoogle']);
    // Route::get('/login-with-google', [AuthController::class, 'googleLoginRedirect']);
});

Route::group([
    'prefix' => 'members',
], function() {
    Route::get('', [MemberController::class, 'getMembers']);
});
Route::post(MembersController::class);
Route::get(MembersController::class);
