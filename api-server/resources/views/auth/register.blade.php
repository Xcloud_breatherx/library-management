<x-auth-layout>
    <div class="card-body register-card-body">
        <p class="login-box-msg">Register a new membership</p>

        <form action="/auth/register" method="post">
            @csrf
          <div class="input-group mb-3 has-validation">
            <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Full name" value="{{old('name')}}" name="name">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
            <div class="invalid-feedback">
                {{$errors->first('name')}}
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control  @error('fathers_name') is-invalid @enderror" placeholder="Father's name" value="{{old('fathers_name')}}" name="fathers_name">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-users"></span>
              </div>
              <div class="invalid-feedback">
                {{$errors->first('fathers_name')}}
            </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control @error('phone_number') is-invalid @enderror" placeholder="Mobile Number" value="{{old('phone_number')}}" name="phone_number">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-phone"></span>
              </div>
            </div>
            <div class="invalid-feedback">
                {{$errors->first('phone_number')}}
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{old('email')}}" name="email">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
            <div class="invalid-feedback">
                {{$errors->first('email')}}
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
            <div class="invalid-feedback">
                {{$errors->first('password')}}
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Retype password" name="password_confirmation">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
            <div class="invalid-feedback">
                {{$errors->first('password')}}
            </div>
          </div>
          <div class="row">
            <div class="col-8">
              <div class="icheck-primary">
                <input type="checkbox" id="agreeTerms" name="terms" value="yes" class="form-check-input @error('terms') is-invalid @enderror">
                <label for="agreeTerms">
                 I agree to the <a href="#">terms</a>
                </label>
                <div class="invalid-feedback">
                    {{$errors->first('terms')}}
                </div>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-4">
              <button type="submit" class="btn btn-primary btn-block">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>

        <div class="social-auth-links text-center">
          <p>- OR -</p>

          <a href="#" class="btn btn-block btn-danger">
            <i class="fab fa-google-plus mr-2"></i>
            Sign up using Google+
          </a>
        </div>

        <a href="/auth/login/" class="text-center">I already have a membership</a>
      </div>
      <!-- /.form-box -->
</x-auth-layout>
